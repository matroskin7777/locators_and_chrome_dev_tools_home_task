Feature: Tasks
  As a user
  I want to test email functionality
  So that I can be sure that it works correctly

  Scenario Outline: User wants to send a message using email service
    Given User opens '<homePage>'
    When User enters verification information '<login>' '<password>'
    And User clicks on write a letter button
    Then User enters '<receiver>' information and '<text>'

    Examples:
      | homePage                                                                        | login               | password  | receiver                | text  |
      |https://accounts.ukr.net/login?client_id=9GLooZH9KjbBlWnuLkVX&drop_reason=logout | justforone@ukr.net  | justForOn | matroskin7777@gmail.com | a     |
