package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class MailPage extends BasePage{

    @FindBy(css = "input[type = 'text']")
    private WebElement loginField;

    @FindBy(className = "_2yPTK9xQ")
    private List<WebElement> passwordField;

    @FindBy(xpath = "//button[@type = 'submit']")
    private WebElement submitButton;

    @FindBy(xpath = "//button[text() = 'Написати листа']")
    private WebElement writeLetterButton;

    @FindBy(css = "input[name = 'toFieldInput']")
    private WebElement toWhomField;

    @FindBy(name = "subject")
    private WebElement messageField;

    @FindBy(xpath = "//button[text() = 'Надіслати']")
    private WebElement sendMessageButton;

    public MailPage(WebDriver driver) {
        super(driver);
    }

    public void openStartUrl(final String url){
        driver.get(url);
        waitForPageLoadComplete(WAIT_TIME);
    }

    public void enterVerificationData(final String email, final String password)
    {
        loginField.sendKeys(email);
        passwordField.get(1).sendKeys(password);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", submitButton);
        waitForPageLoadComplete(WAIT_TIME);
        waitForAjaxToComplete(WAIT_TIME/6);
    }

    public void clickWriteLetter(){
        writeLetterButton.click();
        waitForPageLoadComplete(WAIT_TIME);
        waitForAjaxToComplete(WAIT_TIME/6);
    }

    public void enterInformation(final String receiver, final String text){
        toWhomField.sendKeys(receiver);
        messageField.sendKeys(text);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", sendMessageButton);
    }



}
