package manager;

import org.openqa.selenium.WebDriver;
import pages.MailPage;

public class PageFactoryManager {

    WebDriver driver;

    public PageFactoryManager(WebDriver driver){
        this.driver = driver;
    }

    public MailPage getMailPage(){
        return new MailPage(driver);
    }
}
