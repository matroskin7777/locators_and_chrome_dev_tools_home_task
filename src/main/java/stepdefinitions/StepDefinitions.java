package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.MailPage;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class StepDefinitions {

    PageFactoryManager pageFactoryManager;

    MailPage mailPage;

    WebDriver driver;

    @Before
    public void testsSetup()
    {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @Given("User opens {string}")
    public void openHomePage(final String url){
        mailPage = pageFactoryManager.getMailPage();
        mailPage.openStartUrl(url);
    }

    @When("User enters verification information {string} {string}")
    public void userEntersVerificationInformation(final String login
            ,final String password) {
        mailPage.enterVerificationData(login, password);
    }

    @And("User clicks on write a letter button")
    public void userClicksOnWriteALetterButton() {
        mailPage.clickWriteLetter();
    }

    @Then("User enters {string} information and {string}")
    public void userEntersReceiverInformationAndText(final String receiver
            ,final String text) {
        mailPage.enterInformation(receiver, text);
    }

    @After
    public void tearDown(){
        driver.close();
    }
}
